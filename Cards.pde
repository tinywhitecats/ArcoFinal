/*** buildWall **************************************
 * Purpose: increase wall of target by st and spawn particles
 * Parameters: target - target of building (0 or 1)
 st - amount of wall to build
 * Returns: none
 ******************************************************/
void buildWall(int target, int st)
{
  wall[target]+= st;
  if (st != 0)
  {
    int tH = (int)((wall[0] * 1.0 / 60) * 234);
    for (int i = 0; i < st; i++)
    {
      spawnPart(269 + (400 * target) + random(26), 447 - tH - random(5), 0, 0);
    }
  }
}
/*** buildTower **************************************
 * Purpose: increase tower of target by st and spawn particles
 * Parameters: target - target of building (0 or 1)
 st - amount of tower to build
 * Returns: none
 ******************************************************/
void buildTower(int target, int st)
{
  tower[target] += st;
  if (st != 0)
  {
    int tH = (int)((tower[target] * 1.0 / GMaxTower) * 250); 
    for (int i = 0; i < st; i++)
    {
      spawnPart(160 + (587 * target) + random(50), 447 - tH, 0, 0); //particles
    }
  }
}
/*** damage **************************************
 * Purpose: damage (target) for st - damage wall first
 * Parameters: target - target of attack (0 or 1)
 st - damage
 * Returns: none
 ******************************************************/
void damage(int target, int st) //damages target
{
  int hitWall;
  int hitTower;
  if (st > wall[target]) //hits tower if more damage than target has wall
  {
    tower[target]-= (st - wall[target]);
    wall[target] = 0;
    hitWall = (wall[target]);
    hitTower = (st - wall[target]);
  } else
  {
    hitWall = (st);
    hitTower = 0;
    wall[target]-=st;
  }
  //spawn particles--
  //tower
  int tH;
  if (hitTower != 0)
  {
    tH = (int)((tower[target] * 1.0 / GMaxTower) * 250); 
    for (int i = 0; i < hitTower; i++)
    {
      spawnPart(160 + (587 * target) + random(50), 447 - tH, 0, 0);
    }
  }
  //wall
  if (hitWall != 0)
  {
    tH = (int)((wall[0] * 1.0 / 60) * 234);
    for (int i = 0; i < hitWall; i++)
    {
      spawnPart(269 + (400 * target) + random(26), 447 - tH - random(5), 0, 0);
    }
  }
}
/*** damageTower **************************************
 * Purpose: damage (target)'s tower for (st)
 * Parameters: target - target of attack (0 or 1)
 st - amount of damage
 * Returns: none
 ******************************************************/
void damageTower(int target, int st) //attack target tower for st
{
  tower[target]-= st; //lel
  int tH;
  for (int i = 0; i < st; i++)
  {
    tH = (int)((tower[target] * 1.0 / GMaxTower) * 250); 
    spawnPart(160 + (587 * target) + random(50), 447 - tH, 0, 0);
  }
}
/*** resetDeck **************************************
 * Purpose: shuffle discard pile and add it to the deck
 * Parameters: none
 * Returns: none
 ******************************************************/
void resetDeck()
{  
  deck.clear();
  for (int i = 0; i < cardPlayed.size(); i++)
    deck.append(cardPlayed.get(i));
  cardPlayed.clear();
  deck.shuffle();
}
/*** newDeck **************************************
 * Purpose: completely reset the deck, adding all cards
 * Parameters: none
 * Returns: none
 ******************************************************/
void newDeck()
{
  deck.clear();
  cardPlayed.clear();
  for (int i = 0; i < CARD_NO; i++)
    deck.append(i);
}
/*** drawCard **************************************
 * Purpose: return the id of a card to draw and remove it from the deck
 * Parameters: none
 * Returns: newID - id of card being drawn
 ******************************************************/
int drawCard()
{
  if (deck.size() == 0)
    resetDeck();
  int newLoc = (int)(random(0, deck.size()));
  int newID = deck.get(newLoc);
  deck.remove(newLoc);
  return newID;
}
/*** canPlay **************************************
 * Purpose: determine if (player) can play card (id)
 * Parameters: player - player
 id - id of card being played
 * Returns: if can play it or not
 ******************************************************/
boolean canPlay(int id, int player)
{
  int cost = cards[id].stats.cost;
  if (cost < 100)
  {
    if (bricks[player] >= cost)
      return true;
    return false;
  } else if (cost < 200)
  {
    if (gems[player] >= cost - 100)
      return true;
    return false;
  } else
  {
    if (beasts[player] >= cost - 200)
      return true;
    return false;
  }
}
/*** discardCard **************************************
 * Purpose: add a card to the history
 * Parameters: id - id being discarded
 * Returns: none
 ******************************************************/
void discardCard(int id)
{
  cardPlayed.append(id);
  startAnim(id, 1);
}
/*** playCard **************************************
 * Purpose: play card (id) by activating effects of said card
 * Parameters: id - id of card
 player - player of card
 * Returns: none
 ******************************************************/
void playCard(int id, int player)
{
  cardPlayed.append(id);
  int target = abs(player - 1);
  int cost = cards[id].stats.cost;
  if (cards[id].stats.discard)
    gameState++;
  else if (!cards[id].stats.playAgain)
    changeTurn();
  if (cost < 100)
    bricks[player]-=cost;
  else if (cost < 200)
    gems[player]-= (cost - 100);
  else 
  beasts[player]-= (cost - 200);
  switch (id)
  {
  case 0: //brick shortage
    bricks[0] -= 8;
    bricks[1] -= 8;
    if (bricks[0] < 0)
      bricks[0] = 0;
    if (bricks[1] < 0)
      bricks[1] = 0;
    break;
  case 4: //motherlode
    if (quarry[player] < quarry[target])
      quarry[player]+= 2;
    else
      quarry[player]++;
    break;
  case 7: //Copping the Tech
    if (quarry[player] < quarry[target])
      quarry[player] = quarry[target]; //copping the dab
    break;
  case 11: //Foundations
    if (wall[player] == 0)
      wall[player] += 6;
    else
      wall[player] += 3;
    break;
  case 12: //Tremors
    if (wall[player] < 5)
      damage(player, wall[player]);
    else
      damage(player, 5);
    if (wall[target] < 5)
      damage(target, wall[target]);
    else
      damage(target, 5);
    break;
  case 42: //Parity
    if (magic[player] < magic[target])
      magic[player] = magic[target];
    else if (magic[target] > magic[player])
      magic[target] = magic[player];
    break;
  case 46: //CrumbleCake
    bricks[target] -= 6;
    if (bricks[target] < 0)
      bricks[target] = 0;
    break;
  case 58: //Mad Cows are crazy man!
    beasts[player]-=6;
    beasts[target]-=6;
    if (beasts[target] < 0)
      beasts[target] = 0;
    if (beasts[player] < 0)
      beasts[player] = 0;
    break;
  case 74: //Sheep, but with rabies
    beasts[target]-= 3;
    if (beasts[target] < 0)
      beasts[target] = 0;
    break;
  case 75: //Smol Demon Man
    for (int i = 0; i < 2; i++)
    {
      beasts[i]-= 5;
      bricks[i]-= 5;
      gems[i]-=5;
      if ( beasts[i] < 0)
        beasts[i] = 0;
      if ( bricks[i] < 0)
        bricks[i] = 0;
      if (gems[i] < 0)
        gems[i] = 0;
    }
    break;
  case 76: //Spazzerblarg
    if (wall[target] == 0)
      damage(target, 10);
    else
      damage(target, 6);
    break;
  case 78: //Acid Mushrooms
    if (wall[target] > 0)
      damage(target, 10);
    else
      damage(target, 7);
    break;
  case 79: //Land Narwhal
    if (magic[player] > magic[target])
      damage(target, 12);
    else
      damage(target, 8);
    break;
  case 80: //Pointy Bow Men
    if (wall[player] > wall[target])
      damageTower(target, 6);
    else
      damage(target, 6);
    break;
  case 81: //Demonic Stripper
    damageTower(target, 5);
    beasts[target] -=8;
    if (beasts[target] < 0)
      beasts[target] = 0;
    break;
  case 83: //Stealy Man
    //hoo boy
    int getsGem;
    if (gems[target] > 10)
      getsGem = 10;
    else
      getsGem = gems[target];
    int getsBrick;
    if (bricks[target] > 5)
      getsBrick = 5;
    else
      getsBrick = bricks[target];
    bricks[player] += (getsBrick / 2);
    gems[player] += (getsGem / 2);
    gems[target]-= getsGem;
    bricks[target]-= getsBrick;
    break;
  case 85: //Bitey Mc Bloodface
    beasts[target]-=5;
    if (beasts[target] < 0)
      beasts[target] = 0;
    break;
  case 86: //DARGON BLHAARGG
    gems[target]-=10;
    if (gems[target] < 0)
      gems[target] = 0;
    break;
  case 87: //Drown Thy Sorrows
    if (wall[0] == wall[1])
    {
      zoo[1]--;
      zoo[0]--;
      damageTower(0, 2);
      damageTower(1, 2);
    } else if (wall[0] > wall[1])
    {
      zoo[1]--;
      damageTower(1, 2);
    } else
    {
      zoo[0]--;
      damageTower(0, 2);
    }
    break;
  case 88: //Mancave
    if (zoo[player] < zoo[target])
      zoo[player]++;
    break;
  case 90: //H4x
    int temp = wall[1];
    wall[1] = wall[0];
    wall[0] = temp;
    break;
  case 94: //Eye of Sauron
    if (tower[player] > wall[target])
      damageTower(target, 8);
    else
      damage(target, 8);
    break;
  case 96: //Pouch of Parsley
    if (tower[player] < tower[target])
      buildTower(player, 2);
    else
      buildTower(player, 1);
    break;
  case 101: //Man with large pole
    if (wall[player] > wall[target])
      damage(target, 3);
    else
      damage(target, 2);
    break;
  } //bear with me
  //statistic implementation
  buildTower(player, cards[id].stats.tower); //tower
  buildWall(player, cards[id].stats.wall);//wall
  damage(target, cards[id].stats.atk);//attack
  damageTower(target, cards[id].stats.atktower);//attack tower
  damageTower(player, cards[id].stats.slfAtkTower); //attack own tower
  damage(player, cards[id].stats.selfatk);//self attack
  bricks[player] += cards[id].stats.getBrick;//getBrick
  gems[player] += cards[id].stats.getGem;//getGem
  beasts[player] += cards[id].stats.getBeast;//getBeast
  //resource gain
  switch(cards[id].stats.rtype)
  {
  case 1:
    quarry[player]+= cards[id].stats.resource;
    break;
  case 2: 
    magic[player]+= cards[id].stats.resource;
    break;
  case 3:
    zoo[player]+= cards[id].stats.resource;
    break;
  }
  //resource attack
  switch(cards[id].stats.ratktype)
  {
  case 1:
    quarry[target]-= cards[id].stats.ratk;
    break;
  case 2: 
    magic[target]-= cards[id].stats.ratk;
    break;
  case 3:
    zoo[target]-= cards[id].stats.ratk;
    break;
  }
  startAnim(id, 0); //yay
}
