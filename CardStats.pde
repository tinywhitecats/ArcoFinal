class CardStats
{
  int tower, atk, atktower, wall, cost, selfatk, slfAtkTower, getBrick, getGem, getBeast, resource, rtype, ratk, ratktype;
  boolean playAgain, discard;
  void setDefault()
  {    
    getBrick = 0;
    getGem = 0;
    getBeast = 0;
    tower = 0;
    atktower = 0;
    wall = 0;
    selfatk = 0;
    slfAtkTower = 0;
    playAgain = false;
    resource = 0;
    ratktype = 0;
    atk = 0;
    ratk = 0;
    rtype= 0;
    discard = false;
  }
  /*** setStatsHard **************************************
   * Purpose: set the above vars to reflect the effect of the card
   * Parameters: 'id' - id of card
   * Returns: none
   ******************************************************/
  void setStatsHard(int id) //prewritten
  {
    setDefault();
    switch (id)
    {
    case 0: //Brick Shortage ---------
      cost = 0;
      break;
    case 1: //Lucky Cache
      cost = 0;
      getBrick = 2;
      getGem = 2;
      playAgain = true;
      break;
    case 2: //Friendly Terrain
      cost = 1;
      wall = 1;
      playAgain = true;
      break;
    case 3: //Miners
      cost = 3;
      resource = 1;
      rtype = 1;
      break;
    case 4: //Mother Lode
      cost = 4;
      break;
    case 5: //Dwarven Miners
      cost = 7;
      wall = 4;
      rtype = 1;
      resource = 1;
      break;
    case 6: //Work Overtime
      cost = 2;
      wall = 5;
      getGem = -6;
      break;
    case 7: //Copping the Tech
      cost = 5;
      break;
    case 8: //Basic Wall
      cost = 2;
      wall = 3;
      break;
    case 9: //Sturdy Wall
      cost = 3;
      wall = 4;
      break;
    case 10: //Innovations
      cost = 2;
      getGem = 4;
      rtype = 1;
      resource = 1;
      ratktype = 1;
      ratk = -1;
      break;
    case 11: //Foundations
      cost = 3;
      break;
    case 12: //Tremors
      cost = 7;
      playAgain = true;
      break;
    case 13: //Secret Room
      cost = 8;
      resource = 1;
      rtype = 2;
      playAgain = true;
      break;
    case 14: //Earthquake
      cost = 0;
      rtype = 1;
      resource = -1;
      ratktype = 1;
      ratk = 1;
      break;
    case 15: //Big Wall
      cost = 5;
      wall = 6;
      break;
    case 16: //Collapse!
      cost = 4;
      ratktype = 1;
      ratk = 1;
      break;
    case 17: //New Equipment
      cost = 6;
      rtype = 1;
      resource = 2;
      break;
    case 18: //Strip Mine
      cost = 0;
      rtype = 1;
      resource = -1;
      wall = 10;
      getGem = 5;
      break;
    case 19: //Reinforced Wall
      cost = 8;
      wall = 8;
      break;
    case 20: //Portculus
      cost = 9;
      wall = 5;
      rtype = 3;
      resource = 1;
      break;
    case 21: //Crystal Rocks
      cost = 9;
      wall = 7;
      getGem = 7;
      break;
    case 22: //Harmonic Orc
      cost = 11;
      wall = 6;
      tower = 3;
      break;
    case 23: //Mondo Wall
      cost = 13;
      wall = 12;
      break;
    case 24: //Focused Designs
      cost = 15;
      wall = 8;
      tower = 5;
      break;
    case 25: //Great Wall
      cost = 16;
      wall = 15;
      break;
    case 26: //Rock Launcher
      cost = 18;
      wall = 6;
      atk = 10;
      break;
    case 27: //Dragon's Heart
      cost = 24;
      wall = 20;
      tower = 8;
      break;
    case 28: //Forced Labour
      cost = 7;
      wall = 9;
      getBeast = -5;
      break;
    case 29: //Quartz
      cost = 101;
      tower = 1;
      playAgain = true;
      break;
    case 30: //Smoky Quartz
      cost = 102;
      atktower = 1;
      playAgain = true;
      break;
    case 31: //Amethyst
      cost = 103;
      tower = 3;
      break;
    case 32: //Spell Weavers
      resource = 1;
      rtype = 2;
      cost = 103;
      break;
    case 33: //Prism
      cost = 102;
      discard = true;
      break;
    case 34: //Lodestone
      cost = 105;
      tower = 3;
      break;
    case 35: //Solar Flare
      cost = 104;
      tower = 2;
      atktower = 2;
      break;
    case 36: //Crystal Matrix
      cost = 106;
      resource = 1;
      rtype = 2;
      tower = 3;
      atktower = -1;
      break;
    case 37: //Gemstone Flaw
      cost = 102;
      atktower = 3;
      break;
    case 38: //Ruby
      cost = 103;
      tower = 5;
      break;
    case 39: //Gem Spear
      cost = 104;
      atktower = 5;
      break;
    case 40: //Power Burn
      cost = 103;
      slfAtkTower = 5;
      resource = 2;
      rtype = 2;
      break;
    case 41: //Harmonic Vibe
      cost = 107;
      resource = 1;
      rtype = 2;
      tower = 3;
      wall = 3;
      break;
    case 42: //Parity
      cost = 107;
      break;
    case 43: //Emerald
      cost = 106;
      tower = 8;
      break;
    case 44: //Pearl of Wisdom
      cost = 109;
      resource = 1;
      rtype = 2;
      tower = 5;
      break;
    case 45: //Shatterer
      resource = -1;
      rtype = 2;
      atktower = 9;
      cost = 108;
      break;
    case 46: //Crumblestone
      cost = 107;
      tower = 5;
      break;
    case 47: //Sapphire
      cost = 110;
      tower = 11;
      break;
    case 48: //Discord
      cost = 105;
      atktower = 7;
      slfAtkTower = 7;
      rtype = 2;
      resource = -1;
      ratk = 1;
      ratktype = 2;
      break;
    case 49: //Fire Ruby
      cost = 113;
      tower = 6;
      atktower = 4;
      break;
    case 50: //Quarry's Help
      cost = 104;
      getBrick = -10;
      tower = 7;
      break;
    case 51: //Crystal Shield
      cost = 112;
      tower = 8;
      wall = 3;
      break;
    case 52: //Empathy Gem
      cost = 114;
      tower = 8;
      resource = 1;
      rtype = 3;
      break;
    case 53: //Diamond
      cost = 116;
      tower = 15;
      break;
    case 54: //Sanctuary
      cost = 115;
      tower = 10;
      wall = 5;
      getBeast = 5;
      break;
    case 55: //Lava Jewel
      cost = 117;
      tower = 12;
      atk = 6;
      break;
    case 56: //Dragon's Eye
      cost = 121;
      tower = 20;
      break;
    case 57: //Crystallize
      cost = 108;
      wall = -8;
      tower = 11;
      break;
    case 58: //Mad Cow Disease
      cost = 200;
      break;
    case 59: //Faerie
      cost = 201;
      playAgain = true;
      atk = 2;
      break;
    case 60: //Moody Goblins
      cost = 201;
      atk = 4;
      getGem = -3;
      break;
    case 61: //Husbandry
      cost = 203;
      rtype = 3;
      resource = 1;
      break;
    case 62: //Elven Scout
      cost = 202;
      discard = true;
      break;
    case 63: //Goblin Mob
      cost = 203;
      atk = 6;
      selfatk = 3;
      break;
    case 64: //Goblin Archers
      cost = 204;
      atktower = 3;
      selfatk = 1;
      break;
    case 65: //Shadow Faerie
      cost = 206;
      playAgain = true;
      atktower = 2;
      break;
    case 66: //Orc
      cost = 203;
      atk = 5;
      break;
    case 67: //Dwarves
      cost = 205;
      atk = 4;
      wall = 3;
      break;
    case 68: //Little Snakes
      cost = 206;
      atktower = 4;
      break;
    case 69: //Troll Keeper
      cost = 207;
      rtype = 3;
      resource = 2;
      break;
    case 70: //Tower Gremlin
      cost = 208;
      atk = 2;
      wall = 4;
      tower = 2;
      break;
    case 71: //Full Moon
      cost = 200;
      rtype = 3;
      resource = 1;
      ratk = -1;
      ratktype = 3;
      getBeast = 3;
      break;
    case 72: //Slasher
      cost = 205;
      atk = 6;
      break;
    case 73: //Ogre
      cost = 206;
      atk = 7;
      break;
    case 74: //Rabid Sheep
      cost = 206;
      atk = 6;
      break;
    case 75: //Imp
      cost = 205;
      atk = 6;
      break;
    case 76: //Spizzer
      cost = 208;
      break;
    case 77: //Werewolf
      cost = 209;
      atk = 9;
      break;
    case 78: //Corrosion Cloud
      cost = 211;
      break;
    case 79: //Unicorn
      cost = 209;
      break;
    case 80: //Elven Archers
      cost = 210;
      break;
    case 81: //Succubus
      cost = 214;
      atktower = 5;
      break;
    case 82: //Rock Stompers
      cost = 211;
      atk = 8;
      ratktype = 1;
      ratk = 1;
      break;
    case 83: //Thief
      cost = 212;
      break;
    case 84: //Stone Giant
      cost = 215;
      atk = 10;
      wall = 4;
      break;
    case 85: //Vampire
      cost = 217;
      atk = 10;
      ratk = 1;
      ratktype = 3;
      break;
    case 86: //Dragon
      cost = 225;
      atk = 20;
      ratk = 1;
      ratktype = 3;
      break;
    case 87: //Flood Water
      cost = 6;
      break;
    case 88: //Barracks
      cost = 10;
      getBeast = 6;
      wall = 6;
      break;
    case 89: //Battlements
      cost = 14;
      wall = 7;
      atk = 6;
      break;
    case 90: //Shift
      cost = 17;
      break;
    case 91: //Rock Garden
      cost = 1;
      wall = 1;
      tower = 1;
      getBeast = 2;
      break;
    case 92: //Rainbow
      cost = 100;
      tower = 1;
      getGem = 3;
      atktower = -3;
      break;
    case 93: //Apprentice
      cost = 105;
      tower = 4;
      getBeast = 4;
      atktower = 2;
      break;
    case 94: //Lightning Shard
      cost = 111;
      break;
    case 95: //Phase Jewel
      cost = 118;
      tower = 13;
      getBeast = 6;
      getBrick = 6;
      break;
    case 96: //Bag of Baubles
      cost = 100;
      //gud
      break;
    case 97: //Gnome
      cost = 202;
      getGem = 1;
      atk = 3;
      break;
    case 98: //Berserker
      cost = 204;
      atk = 8;
      slfAtkTower = 3;
      break;
    case 99: //Warlord
      cost = 213;
      atk = 13;
      getGem = -3;
      break;
    case 100: //Pegasus Lancer
      cost = 218;
      atktower = 12;
      break;
    case 101: //Spearman
      cost = 202;
      break;
    } //it looks rough, but spreading that across several days made it doable
  }
}
class Card
{
  int id;
  PImage image;
  CardStats stats;
  void setStatsHard()
  {
    stats = new CardStats();
    stats.setStatsHard(id);
  }
}
 /*** initCards **************************************
* Purpose: initilize cards[] array and set correct values
* Parameters: none
* Returns: none
******************************************************/
void initCards()
{
  for (int i = 0; i < CARD_NO; i++)
  {
    cards[i] = new Card();
    cards[i].id = i;
    if (i < 102)
    {
      cards[i].setStatsHard();
    }
  }
  PImage raw = loadImage("cards.png");
  for (int y = 0; y < 9; y++)
  {
    for (int x = 0; x < 10; x++)
    {
      if (y * 10 + x < 87)
        cards[y * 10 + x].image = raw.get(x * 96, y * 128, 96, 128);
    }
  }
  for (int i = 0; i < 15; i++) //additional - is it loading 1 twice?
  {
    if (i > 9)
      cards[i + 87].image = raw.get((i * 96) % 960, 1280, 96, 128);
    else
      cards[i + 87].image = raw.get((i * 96) % 960, 1152, 96, 128);
  }
}