/*** game **************************************
 * Purpose: Run all methods assosiated with being in a game
 * Parameters: none
 * Returns: none
 ******************************************************/
void game()
{
  if (gameState != 8)
    checkWin();
  checkRes();
  drawMain();
  parts();
  checkHist();
  if (gameState == 8)
  {
    //game is over
    if (fade < 255)
      fade++;
    println(fade);
    println(didWin);
    switch (didWin)
    {
    case 1:
      tint(255, fade);
      image(victory, 130, 175);
      noTint();
      break;
    case 2:
      tint(255, fade);
      image(defeat, 130, 175);
      noTint();
      break;
    case 3:
      tint(255, fade);
      image(tie, 130, 175);
      noTint();
      break;
    }
  } else
  {
    //attempt play card
    if (gameState > 2 && gameState < 5) //their turn
    {
      aiTurn();
    } else if (gameState == 5)
    {
      anim();
    } else if (gameState == 7)
    {
      fade-= 3;
      if (fade <= 20)
        gameState = 1;
      else
      {
        fill(0, 0, 0, fade);
        noStroke();
        rect(0, 0, width, height);
      }
    }
  }
}
/*** checkWin **************************************
 * Purpose: Check if the game has been won by someone
 * Parameters: none
 * Returns: didWin.
 1 means player won
 2 means enemy won
 3 means both won
 0 means nobody won yet
 ******************************************************/
void checkWin()
{
  boolean won[] = new boolean[2];
  won[0] = false;
  won[1] = false;
  for (int i = 0; i < 2; i++)
  {
    if (tower[i] >= GMaxTower || bricks[i] >= GMaxResource || gems[i] >= GMaxResource || beasts[i] > GMaxResource)
      won[i] = true;
    if (tower[i] <= 0)
      won[(i + 1) % 2] = true;
  }
  if (won[0] && !won[1])
  {
    didWin = 1;
    gameState = 8;
    fade = 0;
  } else if (!won[0] && won[1])
  {
    didWin = 2;
    gameState = 8;
    fade = 0;
  } else if (won[0] && won[1])
  {
    didWin = 3;
    gameState = 8;
    fade = 0;
  }
}
/*** resetGame **************************************
 * Purpose: Reset the game and load fields values
 * Parameters: none
 * Returns: none
 ******************************************************/
void resetGame()
{
  //initials
  hand1.clear();
  hand2.clear();
  GMaxTower = int(InitFields[0].content);
  GMaxResource = int(InitFields[1].content);
  GInitRes = int(InitFields[2].content);
  GInitResS = int(InitFields[3].content);
  GInitTower = int(InitFields[4].content);
  GInitWall = int(InitFields[5].content);
  newDeck();
  gameState = 1; 
  for (int i = 0; i < 6; i++) //draw starting hand
  {
    hand1.append(drawCard()); //draw player card
    hand2.append(drawCard()); //draw ai card
  }
  magic[0] = GInitResS;
  magic[1] = GInitResS;
  quarry[0] = GInitResS;
  quarry[1] = GInitResS;
  zoo[0] = GInitResS;
  zoo[1] = GInitResS;
  beasts[0] = GInitRes;
  beasts[1] = GInitRes;
  gems[1] = GInitRes;
  gems[0] = GInitRes;
  bricks[0] = GInitRes;
  bricks[1] = GInitRes;
  tower[0] =GInitTower;
  wall[0] = GInitWall;
  tower[1] = GInitTower;
  wall[1] = GInitWall;
  turnCount = 0;
  hasPlayed = false;
}
/*** changeTurn **************************************
 * Purpose: Switch whose turn it is
 * Parameters: none
 * Returns: none
 ******************************************************/
void changeTurn()
{
  if (gameState > 2)
  {
    for (int i = 0; i < 2; i++)
    {
      bricks[i]+= quarry[i];
      gems[i]+= magic[i];
      beasts[i]+= zoo[i];
    }
  }
  turnCount++;
  hasPlayed = false;
  if (gameState < 3)
  {
    gameState = 3;
    enemyTurnDelay = TURN_DELAY;
  } else
    gameState = 1;
}
/*** cardSelected **************************************
 * Purpose: Determine what card the mouse is hovering over
 * Parameters: none
 * Returns: int of location or -1, meaning invalid
 ******************************************************/
int cardSelected()
{
  if (mouseX > 147 && mouseY > 550 && mouseX < 810 && mouseY < 675)
  {
    boolean gap = false; //determine if the mouse is in a gap between the cards
    for (int i = 0; i < 5; i++)
    {
      if (DEBUG)
      {
        stroke(255);
        fill(0);
      }
      if (mouseX > 240 + (i * 115) && mouseX < 259 + (i * 115))
        gap = true;
    }
    if (!gap)
    {
      //it is a valid card
      return (int)((mouseX - 126) / 115);
    } else
      return -1;
  } else 
  return -1;
}
/*** checkRes **************************************
 * Purpose: make sure nobodies resources are below minimum
 * Parameters: none
 * Returns: none
 ******************************************************/
void checkRes()
{
  for (int i = 0; i < 2; i++)
  {
    if (zoo[i] < 1)
      zoo[i] = 1;
    if (magic[i] < 1)
      magic[i] = 1;
    if (quarry[i] < 1)
      quarry[i] = 1;
    if (bricks[i] < 0)
      bricks[i] = 0;
    if (gems[i] < 0)
      gems[i] = 0;
    if (beasts[i] < 0)
      beasts[i] = 0;
    if (wall[i] < 0)
      wall[i] = 0;
    if (tower[i] < 0)
      tower[i] = 0;
  }
}
/*** drawMain **************************************
 * Purpose: Draw all things that draw while ingame
 (bg,cards,towers,resources)
 * Parameters: none
 * Returns: none
 ******************************************************/
void drawMain()
{
  image(bg, 0, 0);
  if (gameState == 1 || gameState == 2 || gameState == 7)
  {
    for (int i = 0; i < hand1.size(); i++)
    {
      image(cards[hand1.get(i)].image, 690/hand1.size() + i * 115 + 30, 550);
      if (DEBUG)
      {
        textSize(20);
        fill(255);
        text("ID : " + hand1.get(i), 690/hand1.size() + i * 115 + 30, 550);
      }
      if (!canPlay(hand1.get(i), 0))
      {
        noStroke();
        fill(0, 0, 0, 100);
        rect(690/hand1.size() + i * 115 + 30, 550, 96, 128);
      }
      if (gameState == 2)
      {
        image(discardPic, 690/hand1.size() + i * 115 + 30, 550);
      }
    }
  } else
    for (int i = 0; i < hand2.size(); i ++)
      image(cardBack, 690/hand2.size() + i * 115 + 30, 550);
  textFont(coolvetica); //resource sources
  fill(200, 200, 0);
  text(quarry[0], 18, 145);
  text(magic[0], 18, 260);
  text(zoo[0], 18, 375);
  text(quarry[1], 837, 145);
  text(magic[1], 837, 260);
  text(zoo[1], 837, 375);
  textFont(gnuolane); //resources
  fill(0);
  text (bricks[0], 18, 173);
  text (gems[0], 18, 288);
  text (beasts[0], 18, 403);
  text (bricks[1], 837, 173);
  text (gems[1], 837, 288);
  text (beasts[1], 837, 403);
  textFont(lunchds); //tower and wall
  fill(255);
  text(tower[0], 160, 474);
  text(wall[0], 260, 474);
  text(tower[1], 746, 474);
  text(wall[1], 652, 474);
  textSize(7);
  text("/" + GMaxTower, 199, 477);
  text("/" + GMaxTower, 785, 477);
  //other texts

  //draw towers
  int tH = (int)((tower[0] * 1.0 / GMaxTower) * 250); //this determines the height of the tower
  if (tH < 0) tH = 0;
  PImage towerP = towerPic.get(0, 0, 50, tH);
  image(towerP, 160, 448 - tH);
  image(towerRoof1, 148, 347 - tH);
  tH = (int)((tower[1] * 1.0 / GMaxTower) * 250);//this determines the height of the other tower
  if (tH < 0) tH = 0;
  towerP = towerPic.get(0, 0, 50, tH); //get the new image
  image(towerP, 160 + 587, 449 - tH); //draw it
  image(towerRoof2, 148 + 587, 347 - tH); //draw the roof
  tH = (int)((wall[0] * 1.0 / 60) * 234); //determines wall height
  if (tH > 234) //max height 234, as that is the height of the image
    tH = 234;
  towerP = wallPic.get(0, 0, 26, tH);
  image(towerP, 269, 448 - tH);
  tH = (int)((wall[1] * 1.0 / 60) * 234); //other wall
  if (tH > 234)
    tH = 234;
  towerP = wallPic.get(0, 0, 26, tH);
  image(towerP, 659, 449 - tH);
  //card History
  tint(255, 128);//tint the images half transparent
  image(cardBack, 150, 10);
  if (cardHist.size() != 0)
  {
    for (int i  = 0; i < cardHist.size(); i ++)
    {
      tint(255, 128);
      image(cards[cardHist.get(i)].image, 100 * (i % 5) + 250, 10 + (i / 5) * 128);
      if (cardDiscard.get(i) == 1)
        image(discardPic, 100 * (i % 5) + 250, 10 + (i / 5) * 128);
    }
  }
  noTint();
}
class Particle
{
  float x;
  float y;
  float xVel;
  float yVel;
  int r, g, b;
  Particle(float xx, float yy, float xVell, float yVell)
  {
    int dark = (int) random(-10, 100);
    r = 220 + (int)random(-40, 30) - dark;
    g = 175 + (int)random(-40, 30) - dark;
    b = 25;
    x =xx;
    y = yy;
    xVel = xVell;
    yVel = yVell;
  }
  /*** display **************************************
   * Purpose: draw the particle
   * Parameters: none
   * Returns: none
   ******************************************************/
  void display()
  {
    noStroke();
    fill(r, g, b);
    pushMatrix();
    translate(x, y);
    rotate(random(PI));
    rect(0, 0, 3, 3);
    popMatrix();
  }
  /*** move **************************************
   * Purpose: move the particle
   * Parameters: none
   * Returns: none
   ******************************************************/
  void move()
  {
    x += xVel * PART_SPEED_MULTI;
    y += yVel * PART_SPEED_MULTI;
    xVel /= 1.1;
    yVel++;
  }
}
/*** parts **************************************
 * Purpose: process all particles
 * Parameters: none
 * Returns: none
 ******************************************************/
void parts()
{
  for (int i = Particles.size() - 1; i >= 0; i--)
  {//I cannot use an enchanced loop here because i need to know the index location
    Particle part = Particles.get(i);
    part.move();
    if (part.y > 468)
      Particles.remove(i);
    else
      part.display();
  }
}
/*** spawnPart **************************************
 * Purpose: spawn a particle
 * Parameters: x - x coord
 y - y coord
 xV - x velocity
 yV - y velocity
 * Returns: none
 ******************************************************/
void spawnPart(float x, float y, float xV, float yV)
{
  Particles.add(new Particle(x, y, xV, yV));
}
/*** anim **************************************
 * Purpose: process and draw moving cards
 * Parameters: none
 * Returns: none
 ******************************************************/
void anim()
{
  tint(255, cA);
  image(cards[cID].image, cX, cY);
  if (cD == 1)
  {
    tint(255, 128);
    image(discardPic, cX, cY);
  }
  noTint();
  switch (cS)
  {
  case 1:
    cCount++;
    if (cCount >= WAIT_TIME)
      cS = 2;
    break;
  case 2:
    if (cA > 128)
      if (cA - 5 < 128)
        cA = 128;
      else
        cA-= 5;
    if (cX != cTX)
    {
      if (!(cY + (cTY - ((width / 2) - (96 / 2))) / CARD_SPEED < cTY))
      {
        cX+= (cTX - ((width / 2) - (96 / 2))) / CARD_SPEED;
        cY+= (cTY - ((height / 2) - (128 / 2))) / CARD_SPEED;
      } else
      {
        cX = cTX;
        cY = cTY;
      }
    } else if (cA == 128)
    {
      //done
      gameState = prevState;
      cardHist.append(cID);
      cardDiscard.append(cD);
    }
    break;
  }
}
/*** startAnim **************************************
 * Purpose: setup a moving card
 * Parameters: id - id of card
 discard - was discarded (0,1)(I wanted to use a IntList for simplicity)
 * Returns: none
 ******************************************************/
void startAnim(int id, int discard)
{
  prevState = gameState;
  gameState = 5;
  cS = 1;
  cX = (width / 2) - (96 / 2);
  cY = (height / 2) - (128 / 2);
  cA = 255;
  cCount = 0;
  cTX = 100 * (cardHist.size() % 5) + 250;
  cTY = 10 + (cardHist.size() / 5) * 128;
  cID = id;
  cD = discard;
}
class cardFading
{
  int alpha, id, wasDiscarded;
  float x, y, initX, initY;
  cardFading(int id, float x, float y, int d)
  {
    this.id = id;
    alpha = 128;
    this.x = x;
    this.y = y;
    initX = x;
    initY = y;
    wasDiscarded = d;
  }
  cardFading(cardFading f)
  {
    x = f.x;
    y = f.y;
    initX = f.initX;
    initY = f.initY;
    alpha = f.alpha;
    id = f.id;
    wasDiscarded = f.wasDiscarded;
  }
  /*** display **************************************
   * Purpose: draw moving card
   * Parameters: none
   * Returns: none
   ******************************************************/
  void display()
  {
    tint(255, alpha);
    image(cards[id].image, x, y);
    noTint();
    if (wasDiscarded == 1)
      image(discardPic, x, y);
  } //destination is 150,10
  /*** move **************************************
   * Purpose: move moving card
   * Parameters: none
   * Returns: none
   ******************************************************/
  void move()
  {
    x+= ((150 - initX) / DECK_SPEED);
    y+= ((10 - initY) / DECK_SPEED);
    alpha -= (128 / DECK_SPEED);
  }
}
/*** checkHist **************************************
 * Purpose: process fading cards
 * Parameters: none
 * Returns: none
 ******************************************************/
void checkHist()
{
  for (int i = playedCard.size() - 1; i >= 0; i--)
  { //I have to use a normal loop to remove entries
    cardFading cf = new cardFading(playedCard.get(i));
    if (cf.alpha < 1) 
    {
      playedCard.remove(i);
    }
  }
  for (cardFading cf : playedCard)
  { //And I have to use an enchanced loop to run methods
    cf.display();
    cf.move();
  } //Fun right?
}
/*** clearHist **************************************
 * Purpose: turn the history into fading cards
 * Parameters: none
 * Returns: none
 ******************************************************/
void clearHist()
{
  for (int i = 0; i < cardHist.size(); i++)
  {
    playedCard.add(new cardFading(cardHist.get(i), 100 * (i % 5) + 250, 10 + (i / 5) * 128, cardDiscard.get(i) ));
  }
  cardHist.clear();
  cardDiscard.clear();
}