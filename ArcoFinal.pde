/*VER 2.1 : FIX BUG YAAAAAAY (gnome bug)
the other bug - card discarding ai sucks
the other other bug - floating towers on victory
/*...........-o=o-.
 .......,  /=o=o=o=\
 ......_|\|=o=o=o=o=|    Mrrrooooww
 . __.'  a`\=o=o=o=(`\
 .'.  a 4/`|.-""'` \ \ ;'`)   .---.
 ...\  .'  /   .--'  |_.'   / .-._)
 ....`)  .'   /     /`-.__.' /
 ......`'-.___;     /'-.___.-'
 ..............`"""`     
 */
final boolean DEBUG = false; //debug mode - extra info and testing cheats
final int TURN_DELAY = 15; //delay on ai turn
final int CARD_NO = 102; //number of cards ingame
final int WAIT_TIME = 50; //how long the card pauses in the middle after playing
final float CARD_SPEED = 25; //how many frames it takes a played card to get to the history
final float DECK_SPEED = 10; //how long it takes a historical card to get into deck
final float PART_SPEED_MULTI = 0.2;
float cX, cY, cTX, cTY, fade;
int didWin, enemyTurnDelay, cardSelected, turnCount, gameState, GMaxTower, GMaxResource, GInitRes, GInitResS, GInitTower, GInitWall, prevState, cA, cS, cD, cCount, cID;
int quarry[] = new int[2]; //gameState = ((-)-(-8)) - info, -1,0 - MENU, 1 - you play, 2 - you discard, 3 - they play, 4 - they discard, 5 - animation running,
int magic[] = new int[2]; //6 - fade out, 7 - fade in, 8 - game done
int zoo[] = new int[2];
int gems[] = new int[2];
int beasts[] = new int[2];
int bricks[] = new int[2];
int tower[] = new int[2];
int wall[] = new int[2];
float cardValue[] = new float[6];
boolean hasPlayed, releasedMouse = true;
boolean keys[] = new boolean[256];
boolean  keyReleased[] = new boolean[256];
IntList deck = new IntList();
IntList hand1 = new IntList();
IntList hand2 = new IntList();
IntList cardHist = new IntList();
IntList cardDiscard = new IntList();
IntList cardPlayed = new IntList();
PImage towerPic, towerRoof1, towerRoof2, wallPic, bg, discardPic, cardBack, info, menuBG, victory, defeat, tie;
PFont coolvetica, gnuolane, lunchds;
ArrayList<Particle> Particles = new ArrayList<Particle>();
ArrayList<cardFading> playedCard = new ArrayList<cardFading>();
IntField[] InitFields = new IntField[6];
Card cards[] = new Card[CARD_NO];
Button Buttons[] = new Button[4];
void setup()
{
  size(950, 700); //950,700
  for (int i = 0; i < 256; i++) {
    keys[i] = false;
    keyReleased[i] = true;
  }
  PImage raw = loadImage("spritesheet.png");
  towerPic = raw.get(0, 101, 50, 250);
  wallPic = raw.get(51, 101, 27, 234);
  towerRoof1 = raw.get(0, 0, 75, 101);
  towerRoof2 = raw.get(76, 0, 75, 101);
  bg = loadImage("arcomage.png");
  discardPic = loadImage("discard.png");
  coolvetica = createFont("coolvetica.ttf", 50);
  gnuolane = createFont("gnuolane.ttf", 22);
  lunchds = createFont("lunchds.ttf", 19);
  cardBack = loadImage("cardback.png");
  info = loadImage("info.png");
  victory = loadImage("victory.png");
  defeat = loadImage("defeat.png");
  tie = loadImage("tie.png");
  menuBG = loadImage("bg.png");
  raw = loadImage("buttons.png");
  Buttons[0] = new Button(412, 400, 125, 54, raw.get(0, 0, 125, 54), raw.get(125, 0, 125, 54));
  Buttons[1] = new Button(412, 454, 125, 54, raw.get(0, 108, 125, 54), raw.get(125, 108, 125, 54));
  Buttons[2] = new Button(412, 508, 125, 54, raw.get(0, 162, 125, 54), raw.get(125, 162, 125, 54));
  Buttons[3] = new Button(537, 400, 45, 40, raw.get(250, 0, 45, 40), raw.get(250, 40, 45, 40));
  //end image assets
  initFields(); //create fields
  initCards();//oh god no
  didWin = 0; //yeah u didnt win
  gameState = 0; //set to main menu
}
void draw()
{
  mouseMode();
  clear();
  if (gameState > 0 && gameState != 6)
    game(); //in a game
  else if (gameState > -2)
    menu(); //on menu
  else 
  info(); //on info
  if (DEBUG)
  { //extra info for debugging
    fill(255);
    textSize(25);
    text(gameState, 10, 25);
    text(str(hasPlayed), 10, 50);
    text(mouseX + "," + mouseY, mouseX, mouseY - 50);
  }
}
/*** mouseReleased **************************************
 * Purpose: Play cards, activate buttons, etc
 * Parameters: none
 * Returns: none
 ******************************************************/
void mouseReleased() 
{
  if (gameState > 0 && gameState < 6) //not on menu
  {
    cardSelected = cardSelected(); //find what card your mouse is hovering over
    if (mouseButton == LEFT) //try to play a card
    {
      if (gameState == 1) //make sure its your turn
      {
        if (cardSelected != -1 && canPlay(hand1.get(cardSelected), 0) && releasedMouse) //can you play it
        {
          if (!hasPlayed) //hasPlayed is for determining when to clear the card history - only once
          {
            hasPlayed = true; //will be set back to false when the turn changes
            clearHist(); //clear card history for new cards
          }
          playCard(hand1.get(cardSelected), 0); //play the card
          hand1.remove(cardSelected); //take it out of your hand
          hand1.append(drawCard()); //draw a replacement card
        }
      }
    } else if (mouseButton == RIGHT) //try to discard a card
    {
      if (gameState == 1 || gameState == 2) //make sure its your turn
      {
        if (cardSelected != -1 && hand1.get(cardSelected) != 34) //if you are selecting a card and the card is not "Lodestone"
        {
          if (!hasPlayed)
          {
            hasPlayed = true; //history clearing again
            clearHist();
          }
          if (gameState != 2) //change turn if it was not a "discard then play again" turn
            changeTurn();
          else
            gameState = 1; //play again if it was
          discardCard(hand1.get(cardSelected)); //discard the card
          hand1.remove(cardSelected); //take it out of your hand
          hand1.append(drawCard()); //draw a new card
        }
      }
    }
  } else if (gameState > -2 && gameState < 8) //You are on the menu screens
  {
    if (mouseButton == LEFT)
    {
      if (Buttons[0].selected)
      {
        resetGame();
        gameState = 6;
        fade = 0;
      }
      if (Buttons[1].selected)
      {
        gameState = -2;
      }
      if (Buttons[2].selected)
      {
        exit();
      }
      if (Buttons[3].selected)
      {
        if (gameState == 0)
        {
          gameState  = -1;
        } else 
        {
          gameState = 0;
        }
      }
      if (gameState == -1)
      {
        for (int i = 0; i < 6; i++)
        {
          if (mouseX > InitFields[i].x && mouseY > InitFields[i].y - 20 && mouseX < InitFields[i].x + 55 && mouseY < InitFields[i].y + 5)
          {
            InitFields[i].selected = true;
          } else
            InitFields[i].selected = false;
        }
      }
    }
  } else if (gameState != 8 && gameState > -8)
  {
    gameState--;
  } else if (gameState == -8)
  {
    gameState = 0;
  } else if (fade > 200)
  {
    fade = 0;
    gameState = 0;
  }
}
/*** mousePressed **************************************
 * Purpose: Select int fields
 * Parameters: none
 * Returns: none
 ******************************************************/
void keyPressed()
{
  if (gameState == -1)
  {
    for (int i = 0; i < 6; i++)
    {
      if (InitFields[i].selected && key > 47 && key < 58)
      {
        String newS = str(InitFields[i].content);
        if (newS.length() < 5)
        {
          newS += char(key);
          InitFields[i].content = int(newS);
        }
      } else if (InitFields[i].selected && key == BACKSPACE)
      {
        String str = str(InitFields[i].content);
        InitFields[i].content = int(decreaseString(str));
      }
    }
  }
  if (key == ESC)
  {
    key = 0; //this prevents esc from closing the run window
    if (gameState == 0)
      exit();
    else
      gameState = 0;
  } else if (true)
  {
    if (key == 'g')
      tower[0] = 0;
    else if (key == 'h')
      tower[1] = 0;
    else if (key == 'f')
    {
      tower[1] = 0;
      tower[0] = 0;
    }
  }
}
/*** mouseMode **************************************
 * Purpose: Set the pointer to hand or arrow if selecting anything
 * Parameters: none
 * Returns: none
 ******************************************************/
void mouseMode() //what is this madness
{
  if (gameState > 0)
  {
    if (cardSelected() > -1 && gameState < 3)
      cursor(HAND);
    else
      cursor(ARROW);
  } else if (gameState > -2)
  {
    if (Buttons[0].selected || Buttons[1].selected || Buttons[2].selected || Buttons[3].selected)
      cursor(HAND);
    else
      cursor(ARROW);
  } else
    cursor(HAND);
}
