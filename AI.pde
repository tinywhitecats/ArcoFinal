/*** mouseReleased **************************************
 * Purpose: Either wait or take a turn for the enemy tower
 * Parameters: none
 * Returns: none
 ******************************************************/
void aiTurn() //this runs every time the AI has a turn
{
  if (enemyTurnDelay == 0) //wait for turn delay
  {
    getAiValues(); //determine the relative value of the cards in hand
    boolean canPlay = false; //check if it can play
    for (int i = 0; i < hand2.size(); i++)
    {
      if (canPlay(hand2.get(i), 1)) //it can!
        canPlay = true;
    }
    if (canPlay && gameState == 3) //if it can play and it is it's turn and not being forced to discard
    {
      int best = highestVal(); //best will be the location of the card with the highest value
      if (!hasPlayed)
      {
        hasPlayed = true; //history clearing again
        clearHist();
      }
      if (DEBUG)
        println("Ai is playing " + hand2.get(best));
      playCard(hand2.get(best), 1); //play it
      hand2.remove(best); //get rid of it
      hand2.append(drawCard()); //get a new card
      enemyTurnDelay = TURN_DELAY; //reset delay incase it is "play again"
    } else //it cannot play any cards or is being forced to discard by elven scout or prism
    {
      int worst = lowestVal(); //worst will be loc of lowest val card
      if (DEBUG)
        println("Ai is discarding " + hand2.get(worst));
      if (!hasPlayed)
      {
        hasPlayed = true; //history clearing again
        clearHist();
      }
      if (gameState == 4) //if was being forced to discard
        gameState = 3; //play again, with a regular turn
      else
        changeTurn(); //change to player turn
      discardCard(hand2.get(worst)); //discard it
      enemyTurnDelay = TURN_DELAY; //reset turn delay incase of "play again"
      hand2.remove(worst); // Discard
      hand2.append(drawCard()); //get new card
    }
  } else //if the delay hasnt timed out
  {
    enemyTurnDelay--; //lower it closer to playing
  }
}
/*** mouseReleased **************************************
 * Purpose: Return the lowest value card in the enemy hand
 * Parameters: none
 * Returns: position of the lowest value card 'x'
 ******************************************************/
int lowestVal() //Lodestone exists, need to NOT return it's location
{
  /* What this does is pick the first card in the hand, and compare it to the other cards, saving the lowest value
   However, is will ignore Lodestone (as it cannot be discarded). It is also possible that the first card in it's hand IS Lodestone,
   in which case it will start with it's second card as the default
   */
  if (hand2.get(0) == 34) //if lodestone is the first card
  {
    int x = 1; //default second card
    for (int i = 2; i < hand2.size(); i++)
    {
      if (cardValue[i] < cardValue[x] && hand2.get(i) != 34) //if card in loc 'i' is lower value and not lodestone
        x = i; //save it's location
    }
    return x; //return the highest value location
  } else //the first card is not lodestone and can be used
  {
    int x = 0; //default first card
    for (int i = 1; i < hand2.size(); i++)
    {
      if (cardValue[i] < cardValue[x] && hand2.get(i) != 34) //better not be goddamn lodestone
        x = i;
    }
    return x;
  }
}
/*** highestVal **************************************
 * Purpose: Return position of highest value card in enemy hand
 * Parameters: none
 * Returns: highest value 'x'
 ******************************************************/
int highestVal() //get highest value
{
  int x = -1;
  for (int i = 0; i < hand2.size(); i++) //find a card it can play to use as default card
  {
    if (canPlay(hand2.get(i), 1)) //if found one
    {
      x = i; //use it as default
      break;
    }
  }
  for (int i = 0; i < hand2.size(); i++)
  {
    if (cardValue[i] > cardValue[x] && (canPlay(hand2.get(i), 1))) //pretty much the same as lowestVal()
      x = i;
  }
  return x;
}
/*** getAiValues **************************************
 * Purpose: update the cardValue array for the enemy hand
 * Parameters: none
 * Returns: none
 ******************************************************/
void getAiValues()
{
  for (int i = 0; i < 6; i++)
  {
    cardValue[i] = aiValue(hand2.get(i));
  }
}
/*** aiValue **************************************
 * Purpose: Find the relative value of card with id (id) for the enemy
 * Parameters: id - id of the card
 * Returns: ai value 'value'
 ******************************************************/
float aiValue(int id)
{
  float value = 0;
  switch(id)// Some cards do different things based on other factors. These cards has a special line in here to add or subtract value based on those factors
  { //Each of these cards need these lines
  case 4: //Motherlode
    if (quarry[0] > quarry[1])
      value =  200;
    value =  100;
    break;
  case 8: //Copping the Tech
    if (quarry[0] > quarry[1])
      value =  300;
    else
      value =  -100;
    break;
  case 11: //Foundations
    if (wall[1] == 0)
      value =  6/1.5;
    else
      value =  3/1.5;
    break;
  case 18: //Strip Mine
    if (quarry[1] == 1)
      value =  200;
    else
      value =  0;
    break;
  case 33: //Prism
    value =  100;
  case 42: //Parity
    if (magic[0] > magic[1])
      value =  300;
    else
      value =  -800;
    break;
  case 58: //Mad Cow Disease
    value =  0;
    break;
  case 62: //Elven Scout
    value =  100;
    break;
  case 78: //Corrosion Cloud
    if (wall[0] > 0)
      value =  20/5.5;
    else
      value =  14/5.5;
    break;
  case 79: //Unicorn
    if (magic[1] > magic[0])
      value =  24/4.5;
    else
      value =  16/4.5;
    break;
  case 80: //Elven Archers
    if (wall[1] > wall[0])
      value =  18/5;
    else
      value =  12/5;
    break;
  case 83: //Thief
    value = 20;
    break;
  }
  if (cards[id].stats.cost % 100 == 0 && cards[id].stats.playAgain) //if the card is free and doesnt end your turn
    value+= 1000; //Pretty much no reason not to play it so +1000 Value.
  value+= cards[id].stats.resource * 100; //+amount of resource source increase * 100
  if (tower[1] + cards[id].stats.tower >= GMaxTower) //win by tower by playing
    value+= 10000; //This card would win the game, play ASAP
  else if (tower[1] < 30) //low on tower
    value += 150; //need it more to prevent death +150
  value += cards[id].stats.tower * 3; //+tower increase * 3
  value+= cards[id].stats.wall/(wall[1] * (0.1)); //+wall increase/(current wall / 10)
  switch (cards[id].stats.ratktype) //attack resource
  {
  case 1:
    if (quarry[0] > 1) //make sure they are above minimum and it will actually help
      value += 60 * cards[id].stats.ratk; //+ resource attack value * 60
    else //the card wont lower their resource sources as they already are down to 1
    value -= 60; //-60
  case 2:
    if (magic[0] > 1) //same as previous
      value += 60 * cards[id].stats.ratk;
    else
      value -= 60;
  case 3:
    if (zoo[0] > 1)
      value += 60 * cards[id].stats.ratk;
    else
      value -= 60;
  }
  if (cards[id].stats.atk != 0) //calculate attack bonus
  {
    if (tower[0] + wall[0] - cards[id].stats.atk <= 0) //if kill enemy
    {
      value+= 10000; //play ASAP
      if (cards[id].stats.selfatk - wall[1] > tower[1]) //if this card would tie the game
      {
        if (gems[0] + magic[0] * 5 >= GMaxResource ||bricks[0] + quarry[0] * 5 >= GMaxResource||beasts[0] + zoo[0] * 5 >= GMaxResource)
        { //if the enemy is 5 turns or less from winning by resource
          value+= 20000; //Tie the game to avoid losing
        }
      }
    } else if (wall[0] == 0) //if they have no wall attacking is extra good
      value += cards[id].stats.atk * 3; //+attack * 3
    else //otherwise
    value += cards[id].stats.atk * 2; //+attack * 2
  }
  if (tower[0] - cards[id].stats.atktower >= 0) //if kill enemy via direct tower attack
    value+= 10000; //play ASAP
  else
    value+= cards[id].stats.atktower * 3; //+tower attack * 3
  if (cards[id].stats.playAgain) //if it is a "play again" card +100
    value+= 100;
  if (cards[id].stats.selfatk - wall[1] > tower[1]) //if this card would kill the AI
    value -= 10000; //Never play it
  else
    value -= cards[id].stats.selfatk * 2; //- self damage * 2
  if (cards[id].stats.slfAtkTower > tower[1]) //if this card would kill the AI by direct tower damage
    value -= 10000; //Never play it
  else
    value -= cards[id].stats.slfAtkTower * 2; //- self tower damage * 3
  return value/((cards[id].stats.cost % 100) / 4); //return the final value divided by a quarter the cost
} //glad thats done with