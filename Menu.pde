class IntField
{
  int content;
  boolean selected;
  int x, y;
  IntField(int contentInit, int x, int y)
  {
    content = contentInit;
    selected = false;
    this.x = x;
    this.y = y;
  }
}
/*** initFields **************************************
 * Purpose: initialize fields
 * Parameters: none
 * Returns: none
 ******************************************************/
void initFields()
{
  InitFields[0] = new IntField(200, 60, 530); //max tower
  InitFields[1] = new IntField(600, 60, 600); //max resource
  InitFields[2] = new IntField(20, 200, 530); //initial resource
  InitFields[3] = new IntField(2, 200, 600); //initial resource sources
  InitFields[4] = new IntField(25, 600, 530); //initial tower
  InitFields[5] = new IntField(15, 600, 600); //initial wall
}
/*** fields **************************************
 * Purpose: process all fields
 * Parameters: none
 * Returns: none
 ******************************************************/
void fields()
{
  for (int i = 0; i < 6; i++)
  {
    strokeWeight(1);
    textFont(lunchds);
    stroke(255);
    fill(0);
    rect(InitFields[i].x, InitFields[i].y - 20, 65, 25);
    fill(255);
    if (InitFields[i].content != 0)
      text(InitFields[i].content, InitFields[i].x + 5, InitFields[i].y);
  }
}
/*** menu **************************************
 * Purpose: do all menu related things (draw, check buttons)
 * Parameters: none
 * Returns: none
 ******************************************************/
void menu()
{
  if (gameState == 6)
  {
    translate(0, -fade / 5);
  }
  strokeWeight(1);
  image(menuBG, 0, 0);
  buttons();
  stroke(255);
  fill(255);
  textFont(coolvetica, 30);
  text("Tinywhitecat", 397, 683); //This was my real name when I handed this in
  if (gameState == -1)
  {
    fields();
    textSize(14);
    text("Max Tower", 60, 502);
    text("Max Resources", 50, 572);
    text("Starting Resources", 165, 502);
    text("Starting Spawners", 165, 572);
    text("Starting Tower", 592, 502);
    text("Starting Wall", 594, 573);
  }
  if (gameState == 6)
  {
    fade+= 5;
    if (fade == 255)
      gameState = 7;
    fill(0, 0, 0, fade);
    noStroke();
    rect(0, 0, width, height);
  }
}
class Button
{
  float x, y, wid, hig;
  PImage ImNormal, ImSelected;
  boolean selected;
  Button(float x, float y, float wid, float hig, PImage ImNormal, PImage ImSelected)
  {
    this.x = x;
    this.y = y;
    this.wid = wid;
    this.hig = hig;
    selected = false;
    this.ImNormal = ImNormal;
    this.ImSelected = ImSelected;
  }
  /*** checkSelected **************************************
   * Purpose: cehck if button is being hovered over
   * Parameters: none
   * Returns: none
   ******************************************************/
  void checkSelected()
  {
    if (mouseX > x && mouseY > y && mouseX < x + wid && mouseY < y + hig)
      selected = true;
    else
      selected = false;
  }
  /*** display **************************************
   * Purpose: draw dat button
   * Parameters: none
   * Returns: none
   ******************************************************/
  void display()
  {
    if (selected)
      image(ImSelected, x, y);
    else
      image(ImNormal, x, y);
  }
}
/*** buttons **************************************
 * Purpose: do all button things (draw, checkSelected)
 - and add little box for menu
 * Parameters: none
 * Returns: none
 ******************************************************/
void buttons()
{
  fill(0);
  stroke(255);
  rect(412 - 5, 400 - 5, 120 + 5 * 2, 162 + 5 * 2);
  rect(537 - 3, 400 - 5, 45 + 3, 40 + 5);
  noStroke();
  rect(520 - 3, 401 - 5, 44, 43);
  for (int i = 0; i < Buttons.length; i++)
  {
    Buttons[i].checkSelected();
    Buttons[i].display();
  }
}
/*** info **************************************
 * Purpose: draw a page of info
 * Parameters: none
 * Returns: none
 ******************************************************/
void info()
{
  textFont(coolvetica, 30);
  image(info, 0, 0);
  noStroke();
  fill(0, 0, 0, 240);
  switch(gameState)
  {
  case -2:
    rect(0, 500, width, height - 500);
    fill(200, 200, 200, 255);
    rect(470, 0, 10, height);
    text("This is your side.", 30, 550);
    text("This is the opponent's side.", 600, 550);
    break;
  case -3:
    textSize(15);
    rect(0, 500, width, height - 500);
    noFill();
    stroke(255, 0, 0);
    strokeWeight(10);
    rect(8, 58, 122, 350);
    stroke(#E9FA08); //because yellow
    strokeWeight(4);
    rect(15, 100, 30, 50);
    stroke(200);
    fill(200);
    text("Those are your resources. The opponent has resources too, on the right.", 30, 550);
    text("Each of you have ", 30, 570);
    fill(255, 0, 0);
    text("bricks,", 140, 570); //i was dedicated to make this text coloured.
    fill(0, 0, 255); //It was not worth it.
    text("gems,", 182, 570);
    fill(200);
    text("and", 222, 570);
    fill(0, 255, 0);
    text("beasts.", 250, 570);
    fill(200);
    text("You also have quarries, magic, and zoos, displayed in yellow above them.", 30, 590);
    text("Every turn you gain as many bricks as you have quarries.", 30, 620);
    text("The same happens with magic for gems, and zoos for beasts", 30, 640);
    text("The opponent gains resources from their own sources as well.", 556, 550);
    break;
  case -4:
    rect(0, 500, width, height - 500);
    fill(200, 200, 200, 255);
    textSize(25);
    text("You both have a tower, and a wall.", 30, 550);
    text("The tower is the building with the nice roof.", 30, 580);
    text("The blue one is yours.", 30, 610);
    fill(255, 10, 10);
    text("Protect your tower!", 30, 640);
    fill(200);
    text("Your wall will defend your tower.", 600, 550);
    text("It will absorb some of the damage sent.", 535, 580);
    text("(as long as it still stands)", 680, 610);
    break;
  case -5:
    rect(0, 0, width, 550);
    fill(200);
    textSize(25);
    fill(220);
    text("Your cards are down here.", 320, 530);
    break;
  case -6:
    rect(0, 0, width, height);
    image(cards[35].image, width / 2 - 48, 200);
    textSize(20);
    fill(200, 200, 200, 255);
    text("Here is an example card.", 385, 350);
    text("There is a resource cost on the bottom right of the card.", 255, 380);
    text("The type of resource corresponds to the colour of the card.", 247, 400);
    text("(blue for gems, red for bricks, green for beasts)", 280, 420);
    text("The card describes what it does.", 350, 460);
    break;
  case -7:
    rect(0, 0, width, height - 200);
    fill(200, 255);
    text("Left click cards to play them, right click to discard them.", 140, 530);
    break;
  case -8:
    rect(0, 0, width, height);
    fill(255, 255);
    text("Your goal is to destroy the enemies tower or build yours to 200.", 90, 200);
    text("You will also win if you reach 600 of one resource.", 200, 240);
    text("Good luck!", 420, 360);
    text("Arcomage was originally created by 3DO (now defunct).", 160, 500);
    break;
  }
}
/*** decreaseString **************************************
 * Purpose: Return a string identical to st with the last char removed
 * Parameters: st - inital string
 * Returns: newString - the string with the last char removed
 ******************************************************/
String decreaseString(String st)
{ //I'm really proud of this
  char dst[] = new char [st.length()]; //create array of chars
  st.getChars(0, st.length(), dst, 0); //make array of chars contain string
  String newString = ""; //make a new string
  for (int i = 0; i < st.length() - 1; i++)
  { //add all but one of the characters to the string
    newString+= dst[i];
  } //Return it!
  return (newString);
}
/* It's done.
Woooooooooo! */
